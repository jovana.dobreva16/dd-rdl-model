import os
from string import punctuation

from functionality.utils import PharmaEntity
from functionality.entity_recognizers import AllenNLPRecognizer, SpacyRecognizer, BioBERTRecognizer


class NER():


    __allen_ph_org_model_path = "static/ner-models/allen_ph_org_full/model.tar.gz"
    __allen_drug_model_path = "static/ner-models/allen_drugs_full/model.tar.gz"

    def __init__(self):
        print("== Init NER ==")

        self.__init_default_entities()
        self.__init_default_recognizers()

    def __init_default_entities(self):
        self.__entities = [PharmaEntity.pharma_org, PharmaEntity.drug,PharmaEntity]

    def __init_default_recognizers(self):
        self.__spacy_recognizer = SpacyRecognizer({
            self.__spacy_ph_org_model_path:
                [PharmaEntity.pharma_org],
            self.__spacy_drugs_model_path:
                [PharmaEntity.drug]
        })

        self.__allen_recognizer = AllenNLPRecognizer({
            self.__allen_ph_org_model_path:
                [PharmaEntity.pharma_org],
            self.__allen_drug_model_path:
                [PharmaEntity.drug]
        })

        self.__bio_bert_recognizer = BioBERTRecognizer({})

    def __merge_bio_tag(self, x, y):
        if (x[0] == y[0]):
            return x
        if ((x[0] == "I" and y[0] == "O") or (x[0] == "O" and y[0] == "I")):
            return "I" + (x[1:] if x != "O" else y[1:])
        if ((x[0] == "B" and y[0] == "O") or (x[0] == "O" and y[0] == "B")):
            return "B" + (x[1:] if x != "O" else y[1:])
        if ((x[0] == "B" and y[0] == "I") or (x[0] == "I" and y[0] == "B")):
            return "I" + (x[1:] if x != "O" else y[1:])

        return "B" + (x[1:] if x != "O" else y[1:])

    def __concat_bio_tag(self, x, y):
        if (x == "O" and y == "O"):
            return "O"
        elif (x == "O" and y != "O"):
            return y
        elif (x != "O" and y == "O"):
            return x
        else:
            return "O"

    def __merge_bio_tags_for_entities(self, bio_tags_1, bio_tags_2):
        return [self.__merge_bio_tag(x, y) for (x, y) in zip(bio_tags_1, bio_tags_2)]

    def __concat_bio_tags_diverse(self, bio_tags_1, bio_tags_2):
        return [self.__concat_bio_tag(x, y) for (x, y) in zip(bio_tags_1, bio_tags_2)]

    def __get_entities_positions_from_bio_tags(self, bio_tags, tokens):
        entities = []
        init = 0
        relative_start = 0
        relative_index = 0

        entity = ""
        total_len_previous_tokens = 0
        for index, t in enumerate(tokens):
            if (bio_tags[index][0] == "B"):
                entity = t
                relative_start = total_len_previous_tokens
                relative_index = relative_start + len(t)
            elif (bio_tags[index][0] == "I"):
                entity = entity + " " + t
                relative_index = relative_index + len(t)
            elif (bio_tags[index][0] == "O" and entity != ""):
                entities.append(
                    {'entity': entity, 'type': bio_tags[index - 1][2:], 'start': relative_start, 'end': relative_index})
                entity = ""
            total_len_previous_tokens = total_len_previous_tokens + len(t)

        return list(entities)

    def get_entities_objects_from_bio(self, bio_tags, tokens):
        entities = []

        entity = ""
        for index, t in enumerate(tokens):
            if (bio_tags[index][0] == "B"):
                entity = t
            elif (bio_tags[index][0] == "I"):
                entity = entity + " " + t
            elif (bio_tags[index][0] == "O" and entity != ""):
                if (entity not in [e['entity'] for e in entities]):
                    entities.append({
                        'entity': entity.strip(punctuation),
                        'type': bio_tags[index - 1][2:]
                    })
                entity = ""

        return entities

    def __generate_ner_output(self):
        return None

    def get_entities_from_bio(self, bio_tags, tokens):
        entities = []

        entity = ""
        for index, t in enumerate(tokens):
            if (bio_tags[index][0] == "B"):
                entity = t
            elif (bio_tags[index][0] == "I"):
                entity = entity + " " + t
            elif (bio_tags[index][0] == "O" and entity != ""):
                entities.append(entity)
                entity = ""

        return list(set(entities))

    def name_entities(self, text):
        entities_spacy = self.__spacy_recognizer.recognize_entities(text, [PharmaEntity.pharma_org, PharmaEntity.drug])
        entities_allen = self.__allen_recognizer.recognize_entities(text, [PharmaEntity.pharma_org, PharmaEntity.drug])

        # entities_bio_bert_all = self.__bio_bert_recognizer.recognize_entities(text, [])
        # entities_bio_bert = {
        #     'tokens': entities_bio_bert_all['tokens'],
        #     'predictions_bio': self.__merge_bio_tags_for_entities(entities_bio_bert_all['predictions_bio_bionlp3g'], entities_bio_bert_all['predictions_bio_bc5cdr'])
        # }

        # bb_entities = self.get_entities_objects_from_bio(entities_bio_bert['predictions_bio'], entities_bio_bert['tokens'])

        # For different number of tokens (Rare) default to Allen results, as it's more accurate
        if (len(entities_allen['tokens']) != len(entities_spacy['tokens'])):
            ph_orgs = self.get_entities_from_bio(entities_allen['predictions_bio'][0], entities_allen['tokens'])
            drugs = self.get_entities_from_bio(entities_allen['predictions_bio'][1], entities_allen['tokens'])

            custom_entities = [{'entity': p, 'type': 'Pharma_org'} for p in ph_orgs]
            custom_entities = custom_entities + [{'entity': p, 'type': 'Drug'} for p in drugs]

            filtered_bb_entities = []
            # for c in bb_entities:
            #     if(not any([True if c['entity'] in e['entity'] or e['entity'] in c['entity'] else False for e in custom_entities])):
            #         filtered_bb_entities.append(c)

            return {
                "tokens": entities_allen['tokens'],
                "bio_tags_full": self.__concat_bio_tags_diverse(entities_allen['predictions_bio'][0],
                                                                entities_allen['predictions_bio'][1]),
                # "bio_bert_tags": entities_bio_bert,
                "entities": {
                    "custom_entities": custom_entities,
                    # "bb_entities": bb_entities,
                    "filtered_bb_entities": filtered_bb_entities,
                    "all_entities": custom_entities + filtered_bb_entities,
                    "ph_orgs": ph_orgs,
                    "drugs": drugs
                }
            }

        merged_bio_tags_ph_org = self.__merge_bio_tags_for_entities(entities_allen['predictions_bio'][0],
                                                                    entities_spacy['predictions_bio'][0])
        merged_bio_tags_drugs = self.__merge_bio_tags_for_entities(entities_allen['predictions_bio'][1],
                                                                   entities_spacy['predictions_bio'][1])

        ph_orgs = self.get_entities_from_bio(merged_bio_tags_ph_org, entities_spacy['tokens'])
        drugs = self.get_entities_from_bio(merged_bio_tags_drugs, entities_spacy['tokens'])

        custom_entities = [{'entity': p, 'type': 'Pharma_org'} for p in ph_orgs]
        custom_entities = custom_entities + [{'entity': p, 'type': 'Drug'} for p in drugs]

        filtered_bb_entities = []
        # for c in bb_entities:
        #     if(not any([True if c['entity'] in e['entity'] or e['entity'] in c['entity'] else False for e in custom_entities])):
        #         filtered_bb_entities.append(c)

        return {
            "tokens": entities_spacy['tokens'],
            "bio_tags_full": self.__concat_bio_tags_diverse(merged_bio_tags_ph_org, merged_bio_tags_drugs),
            "bio_bert_tags": [],
            "entities": {
                "custom_entities": custom_entities,
                # "bb_entities": bb_entities,
                "filtered_bb_entities": filtered_bb_entities,
                "all_entities": custom_entities + filtered_bb_entities,
                "ph_orgs": ph_orgs,
                "drugs": drugs
            }
        }
