import pandas as pd
import numpy as np
from scipy.spatial.distance import squareform
from scipy.spatial.distance import pdist


def generate_filter(data_path,path_dest,axis):
  data = pd.read_csv(data_path)
  top_data =  pd.crosstab( data['Disease or Syndrome'],data['Pharmacologic Substance'])
  top_data = top_data.apply(lambda x : x / x.sum(),axis=axis)
  top_data.to_csv(path_dest, index=True)

def generate_matrix_2D(data_path,path_dest,axis):
  data = pd.read_csv(data_path)
  top_data =  pd.crosstab( data['Verb'],data['Pharmacologic Substance'])
  top_data = top_data.apply(lambda x : x / x.sum(),axis=axis)
  top_data.to_csv(path_dest, index=True)



def generate_matrix_3D(data_path,path_dest,axis):
  data = pd.read_csv(data_path)
  top_data =  pd.crosstab( [data['Verb'],data['Disease or Syndrome']],data['Pharmacologic Substance'])
  top_data = top_data.apply(lambda x : x / x.sum(),axis=axis)
  top_data.to_csv(path_dest, index=True)

def prune_matrix(matrix_path,matrix_path_final,flag):
  #flag -> 0 if is 2d matrix or flag -> 1 if is 3d matrix
  d = pd.read_csv(matrix_path)
  column = list(d.columns.values)
  maxs = np.max(d.to_numpy(), axis=0)
  pom = 1
  if(flag == 1):
    pom = 2
  d1 = d[column[pom:]].copy()
  avg = np.average(d1.to_numpy())
  print(avg)
  column = list(d.columns.values)
  max_rows = np.max(d1.to_numpy(), axis=1)
  indexes = list(d.index.values)
  for i in range(pom, len(column)):
    if maxs[i] < avg:
      columns_pom = list(d.columns.values)
      if len(columns_pom) > 2:
        d = d.drop(columns=[column[i]])


  for i in range(1, len(indexes)):
    if max_rows[i] < avg:
      if len(d) > 1:
        d = d.drop(index=indexes[i])
  d.to_csv(matrix_path_final, index=False)


def prune_matrix_final(matrix_path,matrix_path_final,flag):
  #flag -> 0 if is 2d matrix or flag -> 1 if is 3d matrix
  d = pd.read_csv(matrix_path)
  column = list(d.columns.values)
  maxs = np.max(d.to_numpy(), axis=0)
  pom = 1
  if(flag == 1):
    pom = 2
  for i in range(pom, len(column)):
    if maxs[i] < 0.7:
      d = d.drop(columns=[column[i]])
  column = list(d.columns.values)
  d1 = d[column[pom:]].copy()
  max_rows = np.max(d1.to_numpy(), axis=1)
  indexes = list(d.index.values)
  for i in range(1, len(indexes)):
    if max_rows[i] < 0.7:
      d = d.drop(index=indexes[i])
  d.to_csv(matrix_path_final, index=False)       




def filtring_triples(path_triples, path_filter, path_final):
  #path filter is 2d matrix that is pruned with the most frequent ones (csv file)
  selected_data = pd.read_csv(path_filter)
  data = pd.read_csv(path_triples)

  pharma_substances = list(selected_data.columns.values)[1:]
  diseases = selected_data['Disease or Syndrome'].to_list()
  print(pharma_substances)

  verbs = data['Verb'].to_list()
  diseases_data = data['Disease or Syndrome'].to_list()
  pharma_substances_data = data['Pharmacologic Substance'].to_list()

  matrix = []

  for i in range(0, len(verbs)):
    if diseases_data[i] in diseases and pharma_substances_data[i] in pharma_substances:
      print([verbs[i], diseases_data[i], pharma_substances_data[i]])
      matrix.append([verbs[i], diseases_data[i], pharma_substances_data[i]])

  df = pd.DataFrame(np.array(matrix), columns=['Verb', 'Disease or Syndrome', 'Pharmacologic Substance'])
  df.to_csv(path_final, index=False)

def generate_probability_matrix(path_2d_matrix, path_3d_matrix,path_probability_matrix):
  data_verbs = pd.read_csv(path_2d_matrix)
  data_triples = pd.read_csv(path_3d_matrix)
  verbs = data_verbs['Verb']
  verbs_triples = data_triples['Verb']
  columns = list(data_verbs.columns.values)
  columns_final = list(data_triples.columns.values)
  diseases = list(data_triples['Disease or Syndrome'])
  matrix = []
  for i in range(0, len(verbs)):
    verb = verbs[i]
    for j in range(0, len(verbs_triples)):

      if verbs_triples[j] == verb:
        array = []
        for k in range(1, len(columns)):
          element = data_triples[columns[k]][j]
          element2 = data_verbs[columns[k]][i]
          final = element / element2
          array.append(final)
        x = [verbs_triples[j], diseases[j]]
        x.extend(array)
        matrix.append(x)
  df = pd.DataFrame(np.array(matrix), columns=columns_final)
  df = df.replace('nan', 0)
  df.to_csv(path_probability_matrix, index=True)

