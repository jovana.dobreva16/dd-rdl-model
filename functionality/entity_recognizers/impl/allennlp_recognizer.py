from typing import Dict, List
from allennlp.predictors.predictor import Predictor
from allennlp.predictors.sentence_tagger import SentenceTaggerPredictor
from allennlp.models.archival import load_archive
from allennlp.data.dataset_readers.dataset_reader import DatasetReader

from functionality.utils import PharmaEntity
from ..abstract_recognizer import AbstractRecognizer
from .reader import PhDatasetReader

class AllenNLPRecognizer(AbstractRecognizer):

    def __init__(self, models_and_entities: Dict[str, List[PharmaEntity]] = {'': []}):
        super().__init__(models_and_entities)
        self.__init_predictors()

    def __init_predictors(self):
        self.__predictors = {}

        for model_path in self._models_and_entites:
            archive = load_archive(model_path)
            archive.model.eval()
            p = Predictor.from_archive(archive)
            self.__predictors[model_path] = p

    def recognize_entities(self, sentence: str, entities_subset= []):
        if(len(entities_subset) == 0):
            return None

        predictions = {
            'tokens': [],
            'predictions_bio': []
        }
        
        for p in self.__predictors:
            l = list(set(self._models_and_entites[p]) & set(entities_subset))
            
            if(len(l) == 0):
                continue
                
            pred = self.__predictors[p].predict(sentence = self._clean_text(sentence))
            cleaned_tags_bioul_to_bio = [ "B" + tag[1:] if tag[0] == "U" else ("I" + tag[1:] if tag == "L" + tag[1:] else tag) for tag in pred['tags'] ]
            predictions['tokens'] = pred['words']
            predictions['predictions_bio'].append(cleaned_tags_bioul_to_bio)

        return predictions
