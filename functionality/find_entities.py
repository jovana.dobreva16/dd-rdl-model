#generating pharma entities for text
from medcat.cat import CAT
from medcat.utils.vocab import Vocab
from medcat.cdb import CDB
import pandas as pd


def search_deseases(text):
  df = pd.read_csv('data_storage/diseases.csv')
  diseases = df['Disease']
  list_diseases =[]
  for disease in diseases:
    if disease in text:
      list_diseases.append(disease)
  return list_diseases

import pandas as pd
def search_drugs( text):
  df = pd.read_csv('data_storage/drugs.csv')
  drugs = df['Drug']
  list_drugs =[]
  for drug in drugs:
    if drug in text:
      list_drugs.append(drug)
  return list_drugs

def find_entities(title,text,final_path):

    df = pd.DataFrame({'Title': [],
                       'Abstract':[],
                       'Organic Chemical': [],
                       'Disease or Syndrome': [],
                       'Pharmacologic Substance': [],
                       'Fungus': [],
                       'Amino Acid, Peptide, or Protein': [],
                       'Therapeutic or Preventive Procedure': [],
                       })
    vocab = Vocab()

    # Load the vocab model you downloaded
    vocab.load_dict('vocab.dat')

    # Load the cdb model you downloaded
    cdb = CDB()
    cdb.load_dict('cdb-medmen.dat')

    # create cat
    cat = CAT(cdb=cdb, vocab=vocab)

    # Test it

    doc_spacy = cat(text)
    # Print detected entities

    # Or to get a json
    import json

    doc_json = json.loads(cat.get_json(text))
    entities = doc_json['entities']
    final_entities = []
    entities_list = []
    if text != 'Empty':
        for entity in entities:
            if entity['type'] == 'Organic Chemical' or entity['type'] == 'Disease or Syndrome' or entity[
                'type'] == 'Therapeutic or Preventive Procedure' or entity['type'] == 'Pharmacologic Substance' or \
                    entity['type'] == 'Amino Acid, Peptide, or Protein' or entity['type'] == 'Fungus':
                final_entities.append(
                    {'name': entity['source_value'], 'full name': entity['pretty_name'], 'type': entity['type']})
                entities_list.append(entity['source_value'])
        print(final_entities)
        data = []

        row = {'Title': title, 'Abstract': text}
        for ent in final_entities:
            if ent['type'] not in row.keys():
                row[ent['type']] = ent['full name']
            else:
                pom = row[ent['type']]
                pom = pom + ", " + ent['full name']
                row[ent['type']] = pom

        data.append(row)
        print(data)
        df = df.append(data, ignore_index=True, sort=False)

    print(df)
    df.to_csv(final_path, index=False)






