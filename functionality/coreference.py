import logging;
logging.basicConfig(level=logging.INFO)
import neuralcoref
import spacy
from typing import List
nlp = spacy.load('en_core_web_sm')
import neuralcoref
neuralcoref.add_to_pipe(nlp)

class Coref:
    __pipe = "neuralcoref"
    def __init__(self):
        print("==Init Neural Coref==")
        self.__init_spacy_nlp()


    def __init_spacy_nlp(self):
        self.__nlp = spacy.load('en_core_web_sm')

        coref = neuralcoref.NeuralCoref(self.__nlp.vocab, greedyness=0.55)
        self.__nlp.add_pipe(coref, name=self.__pipe)



    def get_dict(self):
        return self.dict
    def __replace_entities_mentions(self, doc, clusters, entities):
        resolved = list(tok.text_with_ws for tok in doc)
        for cluster in clusters:
            checkSet = set([True if (e.lower() in cluster.main.text.lower() or cluster.main.text.lower() in e.lower()) else False for e in entities])
            if(True in checkSet):
                for coref in cluster:
                    if coref != cluster.main:
                        resolved[coref.start] = cluster.main.text + \
                            doc[coref.end-1].whitespace_
                        for i in range(coref.start+1, coref.end):
                            resolved[i] = ""
        return ''.join(resolved)
    def coref(self, text: str, entities: List = []):
        # TODO: Check clusters, replace correferences & replace in text
        # Return text with replaced entities

        doc = self.__nlp(text)
        return {
            "sentence": self.__replace_entities_mentions(doc, doc._.coref_clusters, entities)
        }

    def __replace_in_string(self, original_text, text_span_for_replacement, start_pos, end_pos):
        new_text = original_text[0:start_pos]
        new_text = new_text + text_span_for_replacement
        new_text = new_text + original_text[end_pos:]
        return new_text


    

