import pandas as pd
import numpy as np


def generate_triples(path, final_path):
    file1 = pd.read_csv(path)

    diseases = file1['Disease from srl'].copy()
    pharma_substances = file1['Pharma Substance from srl'].copy()
    verbs = file1['Verb'].copy()
    # diseases-custom = file1['Disease from custom function'].copy()
    # pharma_substances-custom = file1['Pharma Substance from custom function'].copy()

    diseases = diseases.fillna('Empty')
    pharma_substances = pharma_substances.fillna('Empty')
    # diseases-custom = diseases.fillna('Empty')
    # pharma_substances-custom = pharma_substances.fillna('Empty')


    matrix = []
    for i in range(0, len(diseases)):
        if diseases[i] != 'Empty'  and pharma_substances[i] != 'Empty':
            matrix.append([verbs[i], diseases[i], pharma_substances[i]])


    file1 = pd.DataFrame(np.array(matrix), columns=['Verb', 'Disease or Syndrome', 'Pharmaceutical Substance'])
    diseases = file1['Disease or Syndrome'].copy()
    pharma_substances = file1['Pharmaceutical Substance'].copy()
    verbs = file1['Verb'].copy()

    matrix = []
    for i in range(0, len(verbs)):
        disease_list = diseases[i].split(", ")
        pharma_substance_list = pharma_substances[i].split(", ")
        for disease in disease_list:
            for pharma_substance in pharma_substance_list:
                matrix.append([verbs[i], disease, pharma_substance])

    df = pd.DataFrame(np.array(matrix), columns=['Verb', 'Disease or Syndrome', 'Pharmacologic Substance'])
    df.to_csv(final_path, index=False)

