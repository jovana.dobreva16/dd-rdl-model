# full data set for abstracts
from medcat.cat import CAT
from medcat.utils.vocab import Vocab
from medcat.cdb import CDB
import pandas as pd

def search_deseases(text):
  df = pd.read_csv('data_storage/diseases.csv')
  diseases = df['Disease']
  list_diseases =[]
  for disease in diseases:
    if disease in text.lower():
      list_diseases.append(disease)
  return list_diseases

import pandas as pd
def search_drugs( text):
  df = pd.read_csv('data_storage/drugs.csv')
  drugs = df['Drug']
  list_drugs =[]
  for drug in drugs:
    if drug in  text.lower():
      list_drugs.append(drug)
  return list_drugs

def generate_new_columns_from_srl(path_srl, path_final):
    file1 = pd.read_csv(path_srl)

    agents = file1['Agent'].copy()
    patients = file1['Patient'].copy()
    verbs = file1['Verb'].copy()
    abstracts = file1['Abstract']

    agents = agents.fillna('Empty')
    patients = patients.fillna('Empty')

    diseases_srl = []
    pharma_substance_srl = []
    diseases_custom = []
    pharma_substance_custom = []

    vocab = Vocab()

    # Load the vocab model you downloaded
    vocab.load_dict('vocab.dat')

    # Load the cdb model you downloaded
    cdb = CDB()
    cdb.load_dict('cdb-medmen.dat')

    # create cat
    cat = CAT(cdb=cdb, vocab=vocab)

    # Test it
    for i in range(0, len(agents)):
        text = agents[i] + " " + verbs[i] + " " + patients[i] + "."

        # Print detected entities
        d = ""
        p = ""

        import json
        doc_json = json.loads(cat.get_json(text))
        entities = doc_json['entities']
        if agents[i] != 'Empty':

            for entity in entities:
                if entity['type'] == 'Disease or Syndrome':
                    d += str(entity['pretty_name']) + ", "
                if entity['type'] == 'Pharmacologic Substance' or entity['type'] == 'Organic Chemical':
                    p += str(entity['pretty_name']) + ", "

            if p == "" or d == "":
                text_agent = agents[i]
                text_patient = patients[i]
                doc_json = json.loads(cat.get_json(text_agent))
                entities = doc_json['entities']
                if agents[i] != 'Empty':

                    for entity in entities:
                        if entity['type'] == 'Disease or Syndrome':
                            if d == "":
                                d += str(entity['pretty_name']) + ", "
                        if entity['type'] == 'Pharmacologic Substance' or entity['type'] == 'Organic Chemical':
                            if p == "":
                                p += str(entity['pretty_name']) + ", "

                doc_json = json.loads(cat.get_json(text_patient))
                entities = doc_json['entities']
                if patients[i] != 'Empty':

                    for entity in entities:
                        if entity['type'] == 'Disease or Syndrome':
                            if d == "":
                                d += str(entity['pretty_name']) + ", "
                        if entity['type'] == 'Pharmacologic Substance' or entity['type'] == 'Organic Chemical':
                            if p == "":
                                p += str(entity['pretty_name']) + ", "
            diseases_list = ", ".join(search_deseases(text))
            drugs_list = ", ".join(search_drugs(text))
        abstract = abstracts[i]
        diseases_list_1 = ", ".join(search_deseases(abstract))
        drugs_list_1 = ", ".join(search_drugs(abstract))
        diseases_list = diseases_list_1 + diseases_list
        drugs_list = drugs_list_1 + drugs_list
        d+=diseases_list
        p+=drugs_list
        diseases_srl.append(d[:-2])
        pharma_substance_srl.append(p[:-2])
        diseases_custom.append(diseases_list[:-2])
        pharma_substance_custom.append(drugs_list[:-2])

    df = pd.DataFrame({'Verb': file1['Verb'].copy(),
                       'Agent': file1['Agent'].copy(),
                       'Patient': file1['Patient'].copy(),
                       'Disease from srl': diseases_srl,
                       'Pharma Substance from srl': pharma_substance_srl,
                       'Disease from custom function': diseases_custom,
                       'Pharma Substance from custom function': pharma_substance_custom,
                       'Title': file1['Title'].copy(),
                       'Abstract': file1['Abstract'].copy(),
                       'Organic Chemical': file1['Organic Chemical'].copy(),
                       'Disease or Syndrome': file1['Disease or Syndrome'].copy(),
                       'Pharmacologic Substance': file1['Pharmacologic Substance'].copy(),
                       'Fungus': file1['Fungus'].copy(),
                       'Amino Acid, Peptide, or Protein': file1['Amino Acid, Peptide, or Protein'].copy(),
                       'Therapeutic or Preventive Procedure': file1['Therapeutic or Preventive Procedure'].copy()
                       })

    print(df)
    df.to_csv(path_final, index=False)






