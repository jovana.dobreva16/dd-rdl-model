from nltk.corpus import wordnet
from nltk.corpus import wordnet as wn
import pandas as pd
import numpy as np
from nltk.stem.wordnet import WordNetLemmatizer


def calculate_similarity_between_verbs(path, final_path):
    df = pd.read_csv(path,encoding = "utf-8")
    data = []
    verbs = df['Verb']
    verbs = [i.lower() for i in verbs]
    verbs = list(set(verbs))
    pom = []
    for i in range(0,len(verbs)):
      verb1 = verbs[i]
      tmp1 = wn.synsets(verb1)
      print(tmp1)
      flag = 0
      for j in range(0, len(tmp1)):
        if tmp1[j].pos()=='v':
          flag = 1
      if flag  == 1 :
        verb1 = WordNetLemmatizer().lemmatize(verb1.lower(),'v')
        print(verb1)
        pom.append(verb1)

    verbs =list(set(pom))

    for i in range(0,len(verbs)):
      for j in range(i+1,len(verbs)):
        verb1 = verbs[i]
        verb2 = verbs[j]
        # tmp1 = wn.synsets(verb1)
        # tmp2 = wn.synsets(verb2)
        # if len(tmp1)!= 0 and len(tmp2)!=0 and tmp1[0].pos()=='v' and tmp2[0].pos()=='v':
        #   verb1 = WordNetLemmatizer().lemmatize(verb1.lower(),'v')
        #   verb2 = WordNetLemmatizer().lemmatize(verb2.lower(),'v')
        #   print(verb1)
        #   print(verb2)
        v1 = wn.synset(verb1+'.v.01')
        v2 = wn.synset(verb2+'.v.01')
        path = v1.path_similarity(v2)
        data.append([verb1,verb2,path])
    if len(data) == 0:
      data.append(['empty','empty',1])
    df_new = pd.DataFrame(np.array(data),columns=['Verb 1','Verb 2', 'Path'])

    df_new.to_csv(final_path, index=False)





def replace_function(path_verb_distance,path_data_srl,final_path,threshold):
  verbs = pd.read_csv(path_verb_distance)
  data = pd.read_csv(path_data_srl)
  data_verbs1 = data['Verb']
  verb1 = verbs['Verb 1'].to_list()
  verb2 = verbs['Verb 2'].to_list()
  distance = verbs['Path'].to_list()
  data_verbs = " ".join(data_verbs1)
  data_verbs = data_verbs.lower()
  print(data_verbs)
  for i in range(0,len(verb1)):
    if distance[i] >= threshold:
      data_verbs = data_verbs.replace(verb2[i],verb1[i])
  matrix = []
  data_verbs1 = data_verbs.split(" ")
  agents = data['Agent'].to_list()
  patients = data['Patient'].to_list()
  titles = data['Title'].to_list()
  abstracts = data['Abstract'].to_list()
  organics = data['Organic Chemical'].to_list()
  disease = data['Disease or Syndrome'].to_list()
  substances = data['Pharmacologic Substance'].to_list()
  fungus = data['Fungus'].to_list()
  acids = data['Amino Acid, Peptide, or Protein'].to_list()
  therapeutic = data['Therapeutic or Preventive Procedure'].to_list()
  diseases_srl = data['Disease from srl'].to_list()
  pharma_substances_srl = data['Pharma Substance from srl'].to_list()
  diseases_custom = data['Disease from custom function'].to_list()
  pharma_substances_custom = data['Pharma Substance from custom function'].to_list()
  for i in range(0,len(data_verbs1)):
    matrix.append([data_verbs1[i], agents[i], patients[i], titles[i], abstracts[i], organics[i], disease[i], substances[i], fungus[i], acids[i], therapeutic[i], diseases_srl[i], pharma_substances_srl[i], diseases_custom[i],pharma_substances_custom[i]])

  columns = list(data.columns.values)
  df_new = pd.DataFrame(np.array(matrix),columns=columns)

  df_new.to_csv(final_path, index=False)

