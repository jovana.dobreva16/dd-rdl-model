import pandas as pd
import numpy as np
import ast

def add_verb_column(path,final_path):
    df = pd.read_csv(path,  encoding='latin-1')

    data = []
    titles = df['Title']
    abstracts = df['Abstract']
    chemicals = df['Organic Chemical']
    diseases = df['Disease or Syndrome']
    substances = df['Pharmacologic Substance']
    fungus = df['Fungus']
    aminos = df ['Amino Acid, Peptide, or Protein']
    therapies = df['Therapeutic or Preventive Procedure']
    srl = df['SRL'].copy()

    s = ast.literal_eval(srl[0])
    for i in range(0,len(srl)):
      print(i)
      s = ast.literal_eval(srl[i])
      for tripple in s['tripples_default']:
        data.append([tripple['verb_root'],tripple['agent'],tripple['patient'],titles[i],abstracts[i],chemicals[i],diseases[i],substances[i],fungus[i],aminos[i],therapies[i]])
    col = ['Verb','Agent','Patient','Title','Abstract','Organic Chemical','Disease or Syndrome','Pharmacologic Substance','Fungus','Amino Acid, Peptide, or Protein','Therapeutic or Preventive Procedure']

    print(np.array(data))
    df_new = pd.DataFrame(np.array(data),columns=col)

    df_new.to_csv(final_path, index=False)
    # list = [i for i in s['sentences_srl'][0]['verbs'][0]['description'].split(" [")]
    # print(type(s['tripples_filtered'][0]))