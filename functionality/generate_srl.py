#generating coref and srl (as column)

import pandas as pd
from functionality.ner import NER
from functionality.coreference import Coref
from functionality.srl import SRL

def generate_srl(path,final_path):
    srl = SRL()
    srl_list = []
    df = pd.DataFrame({'Title': [],
                       'Abstract': [],
                       'Organic Chemical': [],
                       'Disease or Syndrome': [],
                       'Pharmacologic Substance': [],
                       'Fungus':[],
                       'Amino Acid, Peptide, or Protein': [],
                       'Therapeutic or Preventive Procedure': [],
                       'SRL': []
                       })
    file1 = pd.read_csv(path)
    texts = file1['Abstract'].copy()
    titles = file1['Title'].copy()
    organic_chemical= file1['Organic Chemical'].copy()
    diseases= file1['Disease or Syndrome'].copy()
    ph_substance = file1['Pharmacologic Substance'].copy()
    fungus = file1['Fungus'].copy()
    amino_peptide_protein = file1['Amino Acid, Peptide, or Protein'].copy()
    therapy = file1['Therapeutic or Preventive Procedure'].copy()

    coref = Coref()

    for i in range(0,len(texts)):
        org = [ent for ent in str(organic_chemical[i]).split(", ")]
        ph = [ent for ent in str(ph_substance[i]).split(", ")]
        fung = [ent for ent in str(fungus[i]).split(", ")]
        dis = [ent for ent in str(diseases[i]).split(", ")]
        am = [ent for ent in str(amino_peptide_protein[i]).split(", ")]
        tr = [ent for ent in str(therapy[i]).split(", ")]
        entities=[]
        entities.extend(org)
        entities.extend(dis)
        entities.extend(ph)
        entities.extend(fung)
        entities.extend(am)
        entities.extend(tr)
        text = texts[i]
        new_text = coref.coref(text, entities)
        new_text = new_text['sentence']
        srl_pom = srl.label_text(new_text,entities)
        row = {'Title': titles[i], 'Abstract': texts[i],'Organic Chemical':str(organic_chemical[i]),'Disease or Syndrome':str(diseases[i]),'Pharmacologic Substance':str(ph_substance[i]),'Fungus':str(fungus[i]),'Amino Acid, Peptide, or Protein':str(amino_peptide_protein[i]),'Therapeutic or Preventive Procedure':str(therapy[i]),'SRL':str(srl_pom)}
        data = [row]

        print(data)
        df = df.append(data,ignore_index=True,sort=False)

    df.to_csv(final_path, index = False)




