import pandas as pd
import numpy as np

def get_results(path,path_final,threshold):
  data = pd.read_csv(path)
  verbs = list(data['Verb'])
  diseases = list(data['Disease or Syndrome'])
  pharma_substances = list(data.columns.values)[3:]
  matrix = []
  for pharma in pharma_substances:
    column = data[pharma]
    for i in range(0,len(column)):
      if column[i] >= threshold:
        matrix.append([pharma,verbs[i],diseases[i],str(column[i]*100)+"%."])
        print("The drug/medicine/suplement "+pharma+" "+verbs[i]+" "+diseases[i]+" with effectivness "+str(column[i]*100)+"%.")
  df = pd.DataFrame(np.array(matrix), columns=['Pharmaceutical Substance','Action','Disease','Efectivness'])
  df.to_csv(path_final, index=False)