import pandas as pd
import numpy as np

from functionality.replace_similar_verbs import calculate_similarity_between_verbs,replace_function
from functionality.generate_triples import generate_triples
from functionality.generate_matrix import generate_filter,prune_matrix,prune_matrix_final,filtring_triples,generate_matrix_2D,generate_matrix_3D,generate_probability_matrix
from functionality.generate_results import get_results



def update_data(path_srl):
    data = pd.read_csv('/content/drive/My Drive/model/data_storage/dataset.csv')

    verbs = data['Verb'].to_list()
    agents = data['Agent'].to_list()
    patients = data['Patient'].to_list()
    titles = data['Title'].to_list()
    abstracts = data['Abstract'].to_list()
    organics = data['Organic Chemical'].to_list()
    disease = data['Disease or Syndrome'].to_list()
    substances = data['Pharmacologic Substance'].to_list()
    fungus = data['Fungus'].to_list()
    acids = data['Amino Acid, Peptide, or Protein'].to_list()
    therapies = data['Therapeutic or Preventive Procedure'].to_list()
    diseases_srl = data['Disease from srl'].to_list()
    pharma_substances_srl = data['Pharma Substance from srl'].to_list()

    new_srl = pd.read_csv(path_srl)

    new_verbs = new_srl['Verb'].to_list()
    new_agents = new_srl['Agent'].to_list()
    new_patients = new_srl['Patient'].to_list()
    new_titles = new_srl['Title'].to_list()
    new_abstracts = new_srl['Abstract'].to_list()
    new_organics = new_srl['Organic Chemical'].to_list()
    new_disease = new_srl['Disease or Syndrome'].to_list()
    new_substances = new_srl['Pharmacologic Substance'].to_list()
    new_fungus = new_srl['Fungus'].to_list()
    new_acids = new_srl['Amino Acid, Peptide, or Protein'].to_list()
    new_therapies = new_srl['Therapeutic or Preventive Procedure'].to_list()
    new_diseases_srl = new_srl['Disease from srl'].to_list()
    new_pharma_substances_srl = new_srl['Pharma Substance from srl'].to_list()

    # verbs.extend(new_verbs)
    # agents.extend(new_agents)
    # patients.extend(new_patients)
    # titles.extend(new_titles)
    # abstracts.extend(new_abstracts)
    # organics.extend(new_organics)
    # disease.extend(new_disease)
    # substances.extend(new_substances)
    # fungus.extend(new_fungus)
    # acids.extend(new_acids)
    # therapies.extend(new_therapies)
    # diseases_srl.extend(new_diseases_srl)
    # pharma_substances_srl.extend(new_pharma_substances_srl)

    df = pd.DataFrame({'Verb': verbs,
                       'Agent': agents,
                       'Patient': patients,
                       'Title': titles,
                       'Abstract': abstracts,
                       'Organic Chemical': organics,
                       'Disease or Syndrome': disease,
                       'Pharmacologic Substance': substances,
                       'Fungus': fungus,
                       'Amino Acid, Peptide, or Protein': acids,
                       'Therapeutic or Preventive Procedure': therapies,
                       'Disease from srl': diseases_srl,
                       'Pharma Substance from srl': pharma_substances_srl
                       })

    df.to_csv(r'/content/drive/My Drive/model/data_storage/dataset.csv', index=False)

    calculate_similarity_between_verbs('/content/drive/My Drive/model/data_storage/dataset.csv',
                                       '/content/drive/My Drive/model/data_storage/verbs_distance.csv')

    replace_function('/content/drive/My Drive/model/data_storage/verbs_distance.csv', '/content/drive/My Drive/model/data_storage/dataset.csv',
                     '/content/drive/My Drive/model/data_storage/dataset.csv', 0.7)

    generate_triples('/content/drive/My Drive/model/data_storage/dataset.csv', '/content/drive/My Drive/model/data_storage/dataset_tripples.csv')

    generate_filter('/content/drive/My Drive/model/data_storage/dataset_triples.csv', '/content/drive/My Drive/model/data_storage/matrix_filter.csv', 1)

    prune_matrix_final('/content/drive/My Drive/model/data_storage/matrix_filter.csv', '/content/drive/My Drive/model/data_storage/matrix_filter.csv', 0)

    filtring_triples('/content/drive/My Drive/model/data_storage/dataset_triples.csv', '/content/drive/My Drive/model/data_storage/matrix_filter.csv',
                     '/content/drive/My Drive/model/data_storage/filtred_triples.csv')

    generate_matrix_2D('/content/drive/My Drive/model/data_storage/filtred_triples.csv', '/content/drive/My Drive/model/data_storage/distance_matrix_2d.csv',
                       0)

    generate_matrix_3D('/content/drive/My Drive/model/data_storage/filtred_triples.csv', '/content/drive/My Drive/model/data_storage/distance_matrix_3d.csv',
                       0)

    generate_probability_matrix('/content/drive/My Drive/model/data_storage/distance_matrix_2d.csv',
                                '/content/drive/My Drive/model/data_storage/distance_matrix_3d.csv',
                                '/content/drive/My Drive/model/data_storage/matrix_final.csv')
    
    get_results('/content/drive/My Drive/model/data_storage/matrix_final.csv','/content/drive/My Drive/model/data_storage/results_efectivness.csv',0.7)










